<?php

declare(strict_types=1);

namespace App\Services\HandlerManager;

use App\Dto\StoreTransactionDto;
use App\Models\Transaction;
use App\Repositories\AccountRepository;
use DateTime;

class HandleParametersCreator
{
    public function __construct(
        protected readonly StoreTransactionDto $transactionDto,
        protected readonly AccountRepository $accountRepository
    ) {
    }

    /**
     * @return array<mixed>
     */
    public function create(): array
    {
        $account = $this->accountRepository->getUserAccount(
            $this->transactionDto->currencySymbol,
            $this->transactionDto->userId
        );
        $transaction = $this->initializeTransaction();

        return [$transaction, $account];
    }

    private function initializeTransaction(): Transaction
    {
        $transaction = new Transaction();
        $transaction->amount = $this->transactionDto->amount;
        $transaction->comment = $this->transactionDto->comment;
        $transaction->dueDate = DateTime::createFromFormat('Y-m-d', $this->transactionDto->dueDate)
            ?: new DateTime();

        return $transaction;
    }
}
