<?php

declare(strict_types=1);

namespace App\Services\HandlerManager;

use App\Repositories\AccountRepository;

class HandlerManager
{
    public function __construct(
        private readonly AccountRepository $accountRepository
    ) {
    }

    public function get(string $opcode): HandlerProxy
    {
        $opcodeObject = new OpcodeObject($opcode);

        return new HandlerProxy($opcodeObject, $this->accountRepository);
    }
}
