<?php

declare(strict_types=1);

namespace App\Services\HandlerManager;

use App\Contracts\HandlerContractInterface;
use App\Dto\StoreTransactionDto;
use App\Exceptions\HandlerProxyException;
use App\Models\Transaction;
use App\Repositories\AccountRepository;

class HandlerProxy
{
    private readonly HandlerContractInterface $handler;

    public function __construct(
        public readonly OpcodeObject $opcodeObject,
        private readonly AccountRepository $accountRepository,
    ) {
        $this->handler = $this->createHandler();
    }

    public function handle(StoreTransactionDto $transactionDto): Transaction
    {
        $parametersCreator = new HandleParametersCreator($transactionDto, $this->accountRepository);

        $params = $parametersCreator->create();

        try {
            return $this->handler->handle(...$params);
        } catch (\Throwable $exception) {
            // Log exception
            throw new HandlerProxyException(
                "Failed to handle callback.",
                (int)$exception->getCode(),
                $exception
            );
        }
    }

    protected function createHandler(): HandlerContractInterface
    {
        /** @var class-string<HandlerContractInterface> $handlerClass */
        $handlerClass = $this->opcodeObject->handlerClass;

        if (!class_exists($handlerClass)) {
            throw new \Exception("Opcode handler [{$handlerClass}] is not found");
        }

        return new $handlerClass($this->accountRepository);
    }
}
