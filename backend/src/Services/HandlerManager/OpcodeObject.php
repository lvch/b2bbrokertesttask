<?php

declare(strict_types=1);

namespace App\Services\HandlerManager;

use App\Contracts\HandlerContractInterface;
use Illuminate\Support\Str;

/**
 * @property class-string<HandlerContractInterface> $handlerClass
 */
class OpcodeObject
{
    public string $operation;
    public string $handlerClass;

    public function __construct(
        public readonly string $opcode
    ) {
        $this->parse();
        $this->makeHandlerClass();
    }

    protected function parse(): void
    {
        if (preg_match('/^([a-z0-9_]+)$/', $this->opcode, $matches)) {
            throw new \Exception("Opcode [{$this->opcode}] has invalid format");
        }

        [, $this->operation] = $matches;
    }

    protected function makeHandlerClass(): void
    {
        $this->handlerClass = 'App\\Handlers\\' .
            Str::studly(strtolower($this->operation));
    }
}
