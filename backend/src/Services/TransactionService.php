<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\TransactionServiceInterface;
use App\Dto\GetAccountBalanceDto;
use App\Dto\GetAccountsDto;
use App\Dto\GetAccountTransactionsDto;
use App\Dto\StoreTransactionDto;
use App\Models\Transaction;
use App\Repositories\AccountRepository;
use App\Repositories\TransactionRepository;
use App\Services\HandlerManager\HandlerManager;
use App\TransactionHandler;
use Tightenco\Collect\Support\Collection;

class TransactionService implements TransactionServiceInterface
{
    // perform an operation
    public function createTransaction(StoreTransactionDto $transactionDto): Transaction
    {
        return (new TransactionHandler())->handle($transactionDto, new HandlerManager(new AccountRepository()));
    }

    // get all accounts in the system.
    public function getAccounts(GetAccountsDto $getAccountsDto): Collection
    {
        return (new AccountRepository())->getUserAccounts($getAccountsDto->userId);
    }

    // get the balance of a specific account
    public function getAccountBalance(GetAccountBalanceDto $getAccountBalanceDto): string
    {
        return (new AccountRepository())->getUserAccount(
            $getAccountBalanceDto->currencySymbol,
            $getAccountBalanceDto->userId
        )->amount;
    }

    // get all account transactions sorted by comment in alphabetical order.
    // get all account transactions sorted by date.
    public function getAccountTransactions(GetAccountTransactionsDto $getAccountTransactionsDto): Collection
    {
        return (new TransactionRepository())->getTransactions(
            $getAccountTransactionsDto->accountId,
            $getAccountTransactionsDto->sort,
            $getAccountTransactionsDto->sortDirection
        );
    }
}
