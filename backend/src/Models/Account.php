<?php

declare(strict_types=1);

namespace App\Models;

/**
 * @property int $id
 * @property string $amount
 * @property int $currency_id
 *
 * @property-read Currency $currency
 */
class Account extends Model
{
    public function save(): void
    {
        // TODO: Implement save() method.
    }

    public function currency(): Currency
    {
        // TODO: find currency by currency_id
        /** @phpstan-ignore-next-line  */
        $this->currency = new Currency();
        return $this->currency;
    }
}
