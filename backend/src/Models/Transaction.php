<?php

declare(strict_types=1);

namespace App\Models;

use DateTime;

/**
 * @property int $id
 * @property string $amount
 * @property string|null $comment
 * @property DateTime $dueDate
 * @property int $accountFromId
 * @property int $accountToId
 * @property array $extra
 *
 * @property-read Account $accountFrom
 * @property-read Account $accountTo
 */
class Transaction extends Model
{
    public function accountFrom(): Account
    {
        // associate transaction with accountFrom
        return new Account();
    }

    public function accountTo(): Account
    {
        // associate transaction with accountFrom
        return new Account();
    }

    public function save(): void
    {
        // TODO: Implement save() method.
    }
}
