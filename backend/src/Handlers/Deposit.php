<?php

declare(strict_types=1);

namespace App\Handlers;

use App\Contracts\HandlerContractInterface;
use App\Models\Account;
use App\Models\Transaction;

class Deposit implements HandlerContractInterface
{
    public function handle(Transaction $transaction, Account $account): Transaction
    {
        $account->amount = bcadd($account->amount, $transaction->amount, $account->currency->precision);
        $account->save();
        $transaction->save();

        return $transaction;
    }
}
