<?php

declare(strict_types=1);

namespace App\Handlers;

use App\Contracts\HandlerContractInterface;
use App\Exceptions\NotEnoughMoneyException;
use App\Models\Account;
use App\Models\Transaction;

class Withdraw implements HandlerContractInterface
{
    public function handle(Transaction $transaction, Account $account): Transaction
    {
        if (bccomp($account->amount, $transaction->amount, $account->currency->precision) < 0) {
            throw new NotEnoughMoneyException("Not enough money on account [{$account->id}]");
        }

        $account->amount = bcsub($account->amount, $transaction->amount, $account->currency->precision);
        $account->save();
        $transaction->save();

        return $transaction;
    }
}
