<?php

declare(strict_types=1);

namespace App\Handlers;

use App\Contracts\HandlerContractInterface;
use App\Exceptions\NotEnoughMoneyException;
use App\Models\Account;
use App\Models\Transaction;
use App\Repositories\AccountRepository;

class Transfer implements HandlerContractInterface
{
    public function __construct(
        private readonly AccountRepository $accountRepository
    ) {
    }

    public function handle(Transaction $transaction, Account $account): Transaction
    {
        if (bccomp($account->amount, $transaction->amount, $account->currency->precision) < 0) {
            throw new NotEnoughMoneyException("Not enough money on account [{$account->id}]");
        }

        $extra = $transaction->extra;
        $toAccount = $this->accountRepository->getUserAccount($account->currency->symbol, $extra['toUserId']);

        $toAccount->amount = bcadd($toAccount->amount, $transaction->amount, $account->currency->precision);
        $account->amount = bcsub($account->amount, $transaction->amount, $account->currency->precision);

        $toAccount->save();
        $account->save();
        $transaction->save();

        return $transaction;
    }
}
