<?php

declare(strict_types=1);

namespace App\Dto;

class GetAccountTransactionsDto
{
    public function __construct(
        public readonly int $accountId,
        /** @var string|array<mixed> $sort */
        public readonly string|array $sort = [],
        public readonly string $sortDirection = 'asc'
    ) {
    }
}
