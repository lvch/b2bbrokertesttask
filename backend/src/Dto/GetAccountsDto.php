<?php

declare(strict_types=1);

namespace App\Dto;

class GetAccountsDto
{
    public function __construct(
        public readonly int $userId
    ) {
    }
}
