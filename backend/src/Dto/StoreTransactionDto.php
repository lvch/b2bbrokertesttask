<?php

declare(strict_types=1);

namespace App\Dto;

use App\Traits\ArrayAccessTrait;

class StoreTransactionDto implements \ArrayAccess
{
    use ArrayAccessTrait;

    public function __construct(
        public readonly string $opcode,
        public readonly int $userId,
        public readonly string $currencySymbol,
        public readonly string $amount,
        public readonly string $dueDate,
        public readonly ?string $comment = null,
        /** @var array<mixed> $extra */
        public readonly array $extra = [],
    ) {
    }
}
