<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Account;
use Tightenco\Collect\Support\Collection;

class AccountRepository
{
    public function getUserAccount(string $currencySymbol, int $userId): Account
    {
        // TODO: Find Account in Database
        return new Account();
    }

    public function getUserAccounts(int $userId): Collection
    {
        // TODO: Find Accounts in Database
        return new Collection();
    }
}
