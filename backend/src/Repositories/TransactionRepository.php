<?php

declare(strict_types=1);

namespace App\Repositories;

use Tightenco\Collect\Support\Collection;

class TransactionRepository
{
    /**
     * @param int|null $accountId
     * @param array<mixed>|string $sort
     * @param string $direction
     * @return Collection
     */
    public function getTransactions(
        ?int $accountId = null,
        array|string $sort = [],
        string $direction = 'asc'
    ): Collection {
        // TODO: Find transactions
        return new Collection();
    }
}
