<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Dto\GetAccountBalanceDto;
use App\Dto\GetAccountsDto;
use App\Dto\GetAccountTransactionsDto;
use App\Dto\StoreTransactionDto;
use App\Models\Transaction;
use Tightenco\Collect\Support\Collection;

interface TransactionServiceInterface
{
    public function createTransaction(StoreTransactionDto $transactionDto): Transaction;
    public function getAccounts(GetAccountsDto $getAccountsDto): Collection;
    public function getAccountBalance(GetAccountBalanceDto $getAccountBalanceDto): string;
    public function getAccountTransactions(GetAccountTransactionsDto $getAccountTransactionsDto): Collection;
}
