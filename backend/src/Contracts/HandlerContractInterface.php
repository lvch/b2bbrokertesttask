<?php

declare(strict_types=1);

namespace App\Contracts;

use App\Models\Account;
use App\Models\Transaction;

interface HandlerContractInterface
{
    public function handle(
        Transaction $transaction,
        Account $account
    ): Transaction;
}
