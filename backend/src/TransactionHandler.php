<?php

declare(strict_types=1);

namespace App;

use App\Dto\StoreTransactionDto;
use App\Exceptions\HandlerProxyException;
use App\Models\Transaction;
use App\Services\HandlerManager\HandlerManager;

class TransactionHandler
{
    /**
     * @throws HandlerProxyException
     */
    public function handle(StoreTransactionDto $request, HandlerManager $handlerManager): Transaction
    {
        return $handlerManager->get($request->opcode)->handle($request);
    }
}
