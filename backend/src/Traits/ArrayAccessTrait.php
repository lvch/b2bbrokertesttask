<?php

declare(strict_types=1);

namespace App\Traits;

use ArrayAccess;

/**
 * @mixin ArrayAccess
 */
trait ArrayAccessTrait
{
    public function offsetExists(mixed $offset): bool
    {
        return property_exists($this, $offset);
    }

    public function offsetGet(mixed $offset): mixed
    {
        return $this->offsetExists($offset) ? $this->{$offset} : null;
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->{$offset} = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        if ($this->offsetExists($offset)) {
            $this->{$offset} = null;
        }
    }
}
