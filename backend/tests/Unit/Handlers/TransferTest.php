<?php

declare(strict_types=1);

namespace Tests\Unit\Handlers;

use App\Exceptions\NotEnoughMoneyException;
use App\Handlers\Transfer;
use App\Models\Account;
use App\Models\Transaction;
use App\Repositories\AccountRepository;
use PHPUnit\Framework\TestCase;

class TransferTest extends TestCase
{
    private Transaction $transaction;
    private Account $accountFrom;
    private Account $accountTo;

    public function setUp(): void
    {
        $this->transaction = new Transaction();
        $this->transaction->amount = '100';
        $this->transaction->extra = ['toUserId' => 123];

        $this->accountFrom = new Account();
        $this->accountFrom->id = 1;
        $this->accountFrom->amount = '100';
        $this->accountFrom->currency();
        $this->accountFrom->currency->symbol = 'BTC';
        $this->accountFrom->currency->precision = 18;
        $this->accountFrom->currency->name = 'Bitcoin';

        $this->accountTo = new Account();
        $this->accountTo->id = 2;
        $this->accountTo->amount = '0';
        $this->accountTo->currency();
        $this->accountTo->currency->symbol = 'BTC';
        $this->accountTo->currency->precision = 18;
        $this->accountTo->currency->name = 'Bitcoin';
    }

    // phpcs:ignore
    public function test_success(): void
    {
        $accountRepository = $this->createMock(AccountRepository::class);
        $accountRepository->method('getUserAccount')
            ->willReturn($this->accountTo);

        $result = (new Transfer($accountRepository))->handle($this->transaction, $this->accountFrom);

        $this->assertTrue(bccomp('0.00', $this->accountFrom->amount, 2) === 0);
        $this->assertInstanceOf('App\\Models\\Transaction', $result);
    }

    // phpcs:ignore
    public function test_not_enough_money(): void
    {
        $this->accountFrom->amount = '99';
        $this->expectException(NotEnoughMoneyException::class);
        $this->expectExceptionMessage("Not enough money on account [{$this->accountFrom->id}]");
        $accountRepository = $this->createMock(AccountRepository::class);
        $accountRepository->method('getUserAccount')
            ->willReturn($this->accountTo);

        $result = (new Transfer($accountRepository))->handle($this->transaction, $this->accountFrom);
    }
}
