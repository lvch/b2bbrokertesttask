<?php

namespace Tests\Unit\Handlers;

use App\Exceptions\NotEnoughMoneyException;
use App\Handlers\Withdraw;
use App\Models\Account;
use App\Models\Transaction;
use PHPUnit\Framework\TestCase;

class WithdrawTest extends TestCase
{
    private Transaction $transaction;
    private Account $accountFrom;

    public function setUp(): void
    {
        $this->transaction = new Transaction();
        $this->transaction->amount = '100';
        $this->transaction->extra = ['toUserId' => 123];

        $this->accountFrom = new Account();
        $this->accountFrom->id = 1;
        $this->accountFrom->amount = '100';
        $this->accountFrom->currency();
        $this->accountFrom->currency->symbol = 'BTC';
        $this->accountFrom->currency->precision = 18;
        $this->accountFrom->currency->name = 'Bitcoin';
    }

    // phpcs:ignore
    public function test_success(): void
    {
        $result = (new Withdraw())->handle($this->transaction, $this->accountFrom);

        $this->assertTrue(bccomp('0.00', $this->accountFrom->amount, 2) === 0);
        $this->assertInstanceOf('App\\Models\\Transaction', $result);
    }

    // phpcs:ignore
    public function test_not_enough_money(): void
    {
        $this->expectException(NotEnoughMoneyException::class);
        $this->expectExceptionMessage("Not enough money on account [{$this->accountFrom->id}]");
        $this->accountFrom->amount = '99';
        $result = (new Withdraw())->handle($this->transaction, $this->accountFrom);
    }
}
