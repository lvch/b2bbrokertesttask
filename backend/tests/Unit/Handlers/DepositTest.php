<?php

declare(strict_types=1);

namespace Tests\Unit\Handlers;

use App\Handlers\Deposit;
use App\Models\Account;
use App\Models\Transaction;
use PHPUnit\Framework\TestCase;

class DepositTest extends TestCase
{
    // phpcs:ignore
    public function test_success(): void
    {
        $transaction = new Transaction();
        $transaction->amount = '100';

        $account = new Account();
        $account->amount = '0';
        $account->currency();
        $account->currency->symbol = 'BTC';
        $account->currency->precision = 18;
        $account->currency->name = 'Bitcoin';

        $result = (new Deposit())->handle($transaction, $account);

        $this->assertTrue(bccomp('100.00', $account->amount, 2) === 0);
        $this->assertInstanceOf('App\\Models\\Transaction', $result);
    }
}
