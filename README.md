# Test task for B2BBroker

Hello everybody! My name is Dmitrii and I want to present my solution on your task.
The task was:

```
Implement a set of classes for managing the financialoperations of an account.
There are three types of transactions: deposits, withdrawals and 
transfer from account to account.
The transaction contains a comment, an amount, and a due date.

Required methods:
* get all accounts in the system.
* get the balance of a specific account
* perform an operation
* get all account transactions sorted by comment in alphabetical order.
* get all account transactions sorted by date.

The test task must be implemented without the use of
frameworks and databases. This is necessary in order to see
your coding style, ability to understand and implement the
task and demonstrate your skills.
```

## Create transaction
```plantuml
TransactionService -> TransactionHandler: Handle transaction
TransactionHandler -> HandlerManager: Get handler
HandlerManager -> OpcodeObject: Get opcode object
OpcodeObject -> HandlerManager: Return opcode object
HandlerManager -> HandlerProxy: Get handler proxy
HandlerProxy -> HandlerManager: Return handler proxy
HandlerManager -> TransactionHandler: Return handler proxy
TransactionHandler -> HandlerProxy: Handle transaction
HandlerProxy -> HandleParametersCreator: Create parameters
HandleParametersCreator -> HandlerProxy: Return parameters
HandlerProxy -> HandlerContractInterface: Handle handler by opcode
HandlerContractInterface -> HandlerProxy: Return handled transaction
HandlerProxy -> TransactionHandler: Return handled transaction
TransactionHandler -> TransactionService: Return handled transaction
```

## Get all accounts in the system
```plantuml
TransactionService -> AccountRepository: Get Accounts
AccountRepository -> TransactionService: Return collection of accounts
```

## Get the balance of a specific account
```plantuml
TransactionService -> AccountRepository: Get user account for concrete currency
AccountRepository -> TransactionService: Return user account for concrete currency
```

## Get all account transactions sorted by comment in alphabetical order
```plantuml
TransactionService -> TransactionRepository: Get user transaction with sort
TransactionRepository -> TransactionService: Return user transaction with sort
```

## Get all account transactions sorted by date
```plantuml
TransactionService -> TransactionRepository: Get user transaction with sort
TransactionRepository -> TransactionService: Return user transaction with sort
```

As you can see from diagrams I used a few architecture patterns:
* Proxy
* Strategy
* Repository

### Proxy
With Proxy pattern I build parameters for Handlers. It is good solution, because you
can prepare transaction, user account for processing them into Handler.

### Strategy
I choose this pattern, because we have few logics, that are similar. We should send
opcode and data and Strategy will decide who should process it.

### Repository
I prefer to use Repository, because you don't know what will be as a storage.
Repository should decide how we should get data and from.